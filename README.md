# Power of attorney aggregation service 
This service provides aggregated power of attorney information 

## Services
### Get power of attorney information
* Return list of power of attorneys for authorized user with active account and active credit/debit cards
If account is not active the endpoint will not return any cards (/api/v1/power-of-attorneys)

For more details `http://localhost:8090/swagger-ui.html`

### Pre requirements
1. Git
2. Java 11
3. Maven 3.5.4

### Installation
```aidl
git clone ${repo_url}
cd {project_path}
mvn clean install
mvn spring-boot:run
```

## Next steps (things to improve)
* Error handling for timeouts.
* Filtering by Direction, CardType, Authorization, PeriodUnit
* Authorization. User database with permissions. 
* Containerization.
* Integration tests profile. 
* Pipeline: integration tests, deployment.
