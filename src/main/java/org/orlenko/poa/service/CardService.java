package org.orlenko.poa.service;

import org.orlenko.poa.model.card.Card;

import java.util.Optional;

public interface CardService {

	Optional<Card> getCard(String id);
}
