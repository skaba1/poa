package org.orlenko.poa.service.rest;

import static org.springframework.http.HttpMethod.GET;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.orlenko.poa.configuration.properties.AppProperties;
import org.orlenko.poa.factory.CardServiceFactory;
import org.orlenko.poa.model.PowerOfAttorney;
import org.orlenko.poa.model.PowerOfAttorneyReference;
import org.orlenko.poa.model.PowerOfAttorneyView;
import org.orlenko.poa.model.card.Card;
import org.orlenko.poa.model.card.CardReference;
import org.orlenko.poa.model.type.Authorization;
import org.orlenko.poa.service.AccountService;
import org.orlenko.poa.service.CardService;
import org.orlenko.poa.service.PowerOfAttorneyService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class PowerOfAttorneyRestService implements PowerOfAttorneyService
{

    private static final String POWER_OF_ATTORNEYS = "/power-of-attorneys";
    private static final String SEPARATOR = "/";

    @NonNull
    private final RestTemplate restTemplate;
    @NonNull
    private final AppProperties appProperties;
    @NonNull
    private final AccountService accountService;
    @NonNull
    private final CardServiceFactory cardServiceFactory;

    @Override
    public List<PowerOfAttorneyView> getPowerOfAttorneys()
    {
        return getPowerOfAttorneyReferences()
                .stream()
                .map(this::getPowerOfAttorney)
                .map(this::toView)
                .collect(Collectors.toList());
    }

    @Nullable
    private PowerOfAttorney getPowerOfAttorney(final PowerOfAttorneyReference powerOfAttorneyReference)
    {
        return restTemplate.getForObject(getPowerOfAttorneyByIdUrl(powerOfAttorneyReference.getId()),
                PowerOfAttorney.class);
    }

    @Nullable
    private List<PowerOfAttorneyReference> getPowerOfAttorneyReferences()
    {
        return restTemplate.exchange(
                appProperties.getStoreBaseUrl() + POWER_OF_ATTORNEYS,
                GET,
                null,
                new ParameterizedTypeReference<List<PowerOfAttorneyReference>>()
                {
                }).getBody();
    }

    private Optional<Card> getCard(final CardReference cardReference)
    {
        final CardService cardService = cardServiceFactory
                .getCardServiceByType(cardReference.getType());
        return cardService.getCard(cardReference.getId());
    }

    private PowerOfAttorneyView toView(final PowerOfAttorney powerOfAttorney)
    {
        final PowerOfAttorneyView view = new PowerOfAttorneyView()
                .setGrantee(powerOfAttorney.getGrantee())
                .setGrantor(powerOfAttorney.getGrantor());
        accountService.getAccount(powerOfAttorney.getAccount()).ifPresent(account ->
        {
            view.setAccount(account);
            view.setCards(getCards(powerOfAttorney));
        });
        return view;
    }

    private List<Card> getCards(final PowerOfAttorney powerOfAttorney)
    {
        final Set<String> authorizations = powerOfAttorney
                .getAuthorizations().stream()
                .map(Authorization::name)
                .collect(Collectors.toSet());

        return powerOfAttorney
                .getCards()
                .stream()
                .filter(cardReference -> authorizations.contains(cardReference.getType().name()))
                .map(this::getCard)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private String getPowerOfAttorneyByIdUrl(final String id)
    {
        return appProperties.getStoreBaseUrl() + POWER_OF_ATTORNEYS + SEPARATOR + id;
    }
}
