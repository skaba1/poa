package org.orlenko.poa.service.rest;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.orlenko.poa.configuration.properties.AppProperties;
import org.orlenko.poa.model.card.Card;
import org.orlenko.poa.model.card.CreditCard;
import org.orlenko.poa.model.type.Status;
import org.orlenko.poa.service.CardService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Slf4j
@Service
@RequiredArgsConstructor
public class CreditCardRestService implements CardService {

	private static final String CREDIT_CARDS = "%s/credit-cards/%s";

	@NonNull
	private final RestTemplate restTemplate;
	@NonNull
	private final AppProperties appProperties;

	@Override
	public Optional<Card> getCard(String id) {
		log.trace("Get credit card with id '{}'", id);
		CreditCard card = restTemplate.getForObject(format(CREDIT_CARDS, appProperties.getStoreBaseUrl(), id), CreditCard.class);
		if (card != null && card.getStatus() != null && card.getStatus() == Status.ACTIVE) {
			return of(card);
		}
		log.warn("Credit card with '{}' is invalid!", id);
		return empty();
	}
}
