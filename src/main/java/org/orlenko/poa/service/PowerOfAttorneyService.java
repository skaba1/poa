package org.orlenko.poa.service;

import org.orlenko.poa.model.PowerOfAttorneyView;

import java.util.List;

public interface PowerOfAttorneyService {

	List<PowerOfAttorneyView> getPowerOfAttorneys();
}
