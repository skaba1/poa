package org.orlenko.poa.factory;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.orlenko.poa.model.type.CardType;
import org.orlenko.poa.service.CardService;
import org.orlenko.poa.service.rest.CreditCardRestService;
import org.orlenko.poa.service.rest.DebitCardRestService;
import org.springframework.stereotype.Service;

import static org.orlenko.poa.model.type.CardType.CREDIT_CARD;

@Service
@RequiredArgsConstructor
public class CardServiceFactory {

	@NonNull
	private final CreditCardRestService creditCardRestService;
	@NonNull
	private final DebitCardRestService debitCardRestService;

	public CardService getCardServiceByType(CardType type) {
		if (CREDIT_CARD == type) {
			return creditCardRestService;
		}
		return debitCardRestService;
	}
}
