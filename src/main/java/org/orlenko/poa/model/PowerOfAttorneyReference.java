package org.orlenko.poa.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PowerOfAttorneyReference {
	private String id;
}
