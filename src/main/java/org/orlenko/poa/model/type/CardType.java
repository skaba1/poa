package org.orlenko.poa.model.type;

public enum CardType {
	DEBIT_CARD, CREDIT_CARD;
}
