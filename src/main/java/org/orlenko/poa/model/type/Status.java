package org.orlenko.poa.model.type;

public enum Status {
	ACTIVE, BLOCKED;
}
