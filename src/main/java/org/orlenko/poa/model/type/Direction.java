package org.orlenko.poa.model.type;

public enum Direction {

	GIVEN, RECEIVED;
}
