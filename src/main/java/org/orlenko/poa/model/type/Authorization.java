package org.orlenko.poa.model.type;

public enum Authorization {
	DEBIT_CARD, CREDIT_CARD, VIEW, PAYMENT;
}
