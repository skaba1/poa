package org.orlenko.poa.model;

import java.util.ArrayList;
import java.util.List;

import org.orlenko.poa.model.account.Account;
import org.orlenko.poa.model.card.Card;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PowerOfAttorneyView
{
    private String grantor;
    private String grantee;
    private Account account;
    private List<Card> cards = new ArrayList<>();
}
