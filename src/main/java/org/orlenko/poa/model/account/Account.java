package org.orlenko.poa.model.account;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

@Data
@Accessors(chain = true)
public class Account {
	private String owner;
	private Long balance;
	@JsonFormat(shape = STRING, pattern = "dd-MM-yyyy")
	private LocalDate created;
	@JsonFormat(shape = STRING, pattern = "dd-MM-yyyy")
	private LocalDate ended;
}
