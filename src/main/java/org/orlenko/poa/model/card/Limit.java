package org.orlenko.poa.model.card;

import lombok.Data;
import lombok.experimental.Accessors;
import org.orlenko.poa.model.type.PeriodUnit;

@Data
@Accessors(chain = true)
public class Limit {
	private Integer limit;
	private PeriodUnit periodUnit;
}
