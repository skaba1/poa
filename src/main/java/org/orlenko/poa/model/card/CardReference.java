package org.orlenko.poa.model.card;

import lombok.Data;
import lombok.experimental.Accessors;
import org.orlenko.poa.model.type.CardType;

@Data
@Accessors(chain = true)
public class CardReference {
	private String id;
	private CardType type;
}
