package org.orlenko.poa.model.card;

import lombok.Data;
import lombok.experimental.Accessors;
import org.orlenko.poa.model.type.Status;

@Data
@Accessors(chain = true)
public class Card {
	private String id;
	private Status status;
	private Integer cardNumber;
	private Integer sequenceNumber;
	private String cardHolder;
}
