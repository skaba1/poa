package org.orlenko.poa.model.card;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DebitCard extends Card {
	private Limit atmLimit;
	private Limit posLimit;
	@JsonProperty("contactless")
	private boolean contactLess;
}
