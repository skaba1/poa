package org.orlenko.poa.configuration.properties;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AppProperties {
	private String storeBaseUrl;
}
