package org.orlenko.poa.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.orlenko.poa.configuration.exception.CustomExceptionHandler;
import org.orlenko.poa.exception.ResourceNotFoundException;
import org.orlenko.poa.model.PowerOfAttorneyView;
import org.orlenko.poa.model.account.Account;
import org.orlenko.poa.model.card.Card;
import org.orlenko.poa.model.card.CreditCard;
import org.orlenko.poa.service.PowerOfAttorneyService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class PowerOfAttorneyControllerTest {

	@Mock
	private PowerOfAttorneyService powerOfAttorneyService;
	@InjectMocks
	private PowerOfAttorneyController powerOfAttorneyController;

	private MockMvc mvc;

	@Before
	public void setUp() {
		CustomExceptionHandler customExceptionHandler = new CustomExceptionHandler();
		mvc = MockMvcBuilders.standaloneSetup(powerOfAttorneyController).setControllerAdvice(customExceptionHandler).build();
	}

	@Test
	public void getPowerOfAttorneys() throws Exception {
		Card creditCard = new CreditCard().setMonthlyLimit(25).setId("id").setCardNumber(12345).setCardHolder("holder").setSequenceNumber(1);
		Account account = new Account().setOwner("Owner 1").setBalance(700L).setCreated(LocalDate.parse("2019-10-20")).setEnded(LocalDate.parse("2020-10-20"));
		PowerOfAttorneyView powerOfAttorney = new PowerOfAttorneyView().setGrantor("grantor")
			.setGrantee("grantee")
			.setAccount(account)
			.setCards(singletonList(creditCard));

		when(powerOfAttorneyService.getPowerOfAttorneys()).thenReturn(singletonList(powerOfAttorney));

		mvc.perform(get("/power-of-attorneys").accept(APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(APPLICATION_JSON))
			.andExpect(jsonPath("$[0].grantee", is("grantee")))
			.andExpect(jsonPath("$[0].account.owner", is("Owner 1")))
			.andExpect(jsonPath("$[0].account.balance", is(700)))
			.andExpect(jsonPath("$[0].account.created", is("20-10-2019")))
			.andExpect(jsonPath("$[0].account.ended", is("20-10-2020")))
			.andExpect(jsonPath("$[0].grantor", is("grantor")))
			.andExpect(jsonPath("$[0].cards[0].id", is("id")))
			.andExpect(jsonPath("$[0].cards[0].cardHolder", is("holder")))
			.andExpect(jsonPath("$[0].cards[0].sequenceNumber", is(1)))
			.andExpect(jsonPath("$[0].cards[0].monhtlyLimit", is(25)));
	}

	@Test
	public void getPowerOfAttorneysResourceNotFoundException() throws Exception {
		when(powerOfAttorneyService.getPowerOfAttorneys()).thenThrow(new ResourceNotFoundException("Not Found"));

		mvc.perform(get("/power-of-attorneys").accept(APPLICATION_JSON))
			.andExpect(status().isNotFound());
	}

	@Test
	public void getPowerOfAttorneysInternalServerError() throws Exception {
		when(powerOfAttorneyService.getPowerOfAttorneys()).thenThrow(new RuntimeException("Not Found"));

		mvc.perform(get("/power-of-attorneys").accept(APPLICATION_JSON))
			.andExpect(status().isInternalServerError());
	}
}