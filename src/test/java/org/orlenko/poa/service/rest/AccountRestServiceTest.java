package org.orlenko.poa.service.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.orlenko.poa.model.account.Account;
import org.orlenko.poa.service.AccountService;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Optional;

import static java.lang.String.format;
import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.orlenko.poa.configuration.properties.AppProperties.builder;
import static org.orlenko.poa.service.rest.TestUtils.getAccount;

@RunWith(MockitoJUnitRunner.class)
public class AccountRestServiceTest {

	private static final String HTTP_LOCAL_HOST = "http://localhost:8080";
	private static final String ACCOUNTS = "%s/accounts/%s";
	private static final String ACCOUNT = "NL23RABO123456789";
	private static final String ACCOUNT_ID = "123456789";
	private static final LocalDate ENDED_PAST = now().minusDays(1);
	private static final LocalDate ENDED_FUTURE = now().plusYears(1);

	@Mock
	private RestTemplate restTemplate;

	private AccountService accountService;

	@Before
	public void setUp() {
		accountService = new AccountRestService(restTemplate, builder().storeBaseUrl(HTTP_LOCAL_HOST).build());
	}

	@Test
	public void getAccountEndDateEmpty() {
		Account expectedAccount = getAccount(null);
		when(restTemplate.getForObject(getUrl(), Account.class)).thenReturn(expectedAccount);

		Optional<Account> account = accountService.getAccount(ACCOUNT);
		assertThat(account).isNotEmpty();
		assertThat(account).hasValue(expectedAccount);
	}

	@Test
	public void getAccountEndDateInFuture() {
		Account expectedAccount = getAccount(ENDED_FUTURE);
		when(restTemplate.getForObject(getUrl(), Account.class)).thenReturn(expectedAccount);

		Optional<Account> account = accountService.getAccount(ACCOUNT);
		assertThat(account).isNotEmpty();
		assertThat(account).hasValue(expectedAccount);
	}

	@Test
	public void getAccountEndDateInThePast() {
		Account expectedAccount = getAccount(ENDED_PAST);
		when(restTemplate.getForObject(getUrl(), Account.class)).thenReturn(expectedAccount);

		Optional<Account> account = accountService.getAccount(ACCOUNT);
		assertThat(account).isEmpty();
	}

	private String getUrl() {
		return format(ACCOUNTS, HTTP_LOCAL_HOST, ACCOUNT_ID);
	}
}