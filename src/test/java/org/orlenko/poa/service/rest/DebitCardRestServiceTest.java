package org.orlenko.poa.service.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.orlenko.poa.model.card.Card;
import org.orlenko.poa.model.card.DebitCard;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.orlenko.poa.configuration.properties.AppProperties.builder;
import static org.orlenko.poa.model.type.Status.ACTIVE;
import static org.orlenko.poa.model.type.Status.BLOCKED;
import static org.orlenko.poa.service.rest.TestUtils.CARD_ID_1;
import static org.orlenko.poa.service.rest.TestUtils.getDebitCard;

@RunWith(MockitoJUnitRunner.class)
public class DebitCardRestServiceTest {

	private static final String HTTP_LOCAL_HOST = "http://localhost:8080";
	private static final String DEBIT_CARDS = "%s/debit-cards/%s";

	@Mock
	private RestTemplate restTemplate;

	private DebitCardRestService debitCardRestService;

	@Before
	public void setUp() {
		debitCardRestService = new DebitCardRestService(restTemplate, builder().storeBaseUrl(HTTP_LOCAL_HOST).build());
	}

	@Test
	public void getCard() {
		Card expectedCard = getDebitCard(ACTIVE);
		when(restTemplate.getForObject(getUrl(), DebitCard.class)).thenReturn((DebitCard) expectedCard);

		Optional<Card> card = debitCardRestService.getCard(CARD_ID_1);
		assertThat(card).isNotEmpty();
		assertThat(card).hasValue(expectedCard);
	}

	@Test
	public void getCardIsNotActive() {
		Card expectedCard = getDebitCard(BLOCKED);

		when(restTemplate.getForObject(getUrl(), DebitCard.class)).thenReturn((DebitCard) expectedCard);

		Optional<Card> card = debitCardRestService.getCard(CARD_ID_1);
		assertThat(card).isEmpty();
	}

	private String getUrl() {
		return format(DEBIT_CARDS, HTTP_LOCAL_HOST, CARD_ID_1);
	}
}