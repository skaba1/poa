package org.orlenko.poa.service.rest;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.orlenko.poa.model.type.Authorization.CREDIT_CARD;
import static org.orlenko.poa.model.type.Authorization.DEBIT_CARD;
import static org.orlenko.poa.model.type.Authorization.VIEW;
import static org.orlenko.poa.model.type.Direction.GIVEN;
import static org.orlenko.poa.service.rest.TestUtils.CARD_ID_1;
import static org.orlenko.poa.service.rest.TestUtils.CARD_ID_2;
import static org.orlenko.poa.service.rest.TestUtils.getAccount;
import static org.orlenko.poa.service.rest.TestUtils.getCreditCard;
import static org.orlenko.poa.service.rest.TestUtils.getDebitCard;
import static org.springframework.http.HttpMethod.GET;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.orlenko.poa.configuration.properties.AppProperties;
import org.orlenko.poa.factory.CardServiceFactory;
import org.orlenko.poa.model.PowerOfAttorney;
import org.orlenko.poa.model.PowerOfAttorneyReference;
import org.orlenko.poa.model.PowerOfAttorneyView;
import org.orlenko.poa.model.account.Account;
import org.orlenko.poa.model.card.Card;
import org.orlenko.poa.model.card.CardReference;
import org.orlenko.poa.model.type.Authorization;
import org.orlenko.poa.model.type.CardType;
import org.orlenko.poa.model.type.Status;
import org.orlenko.poa.service.AccountService;
import org.orlenko.poa.service.PowerOfAttorneyService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class PowerOfAttorneyServiceTest
{

    private static final String HTTP_LOCAL_HOST = "http://local§host:8080";
    private static final String POWER_OF_ATTORNEYS_REFERENCE = "/power-of-attorneys";
    private static final String POWER_OF_ATTORNEYS = "/power-of-attorneys/%s";
    private static final String POA_ID = "01";
    private static final String ACCOUNT_ID = "001111";
    private static final String GRANTOR = "Grantor";
    private static final String GRANTEE = "Grantee";

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private AccountService accountService;
    @Mock
    private CardServiceFactory cardServiceFactory;
    @Mock
    private DebitCardRestService debitCardRestService;
    @Mock
    private CreditCardRestService creditCardRestService;

    private PowerOfAttorneyService powerOfAttorneyService;

    @Before
    public void setUp()
    {
        final AppProperties properties = AppProperties.builder().storeBaseUrl(HTTP_LOCAL_HOST).build();
        powerOfAttorneyService = new PowerOfAttorneyRestService(restTemplate, properties, accountService,
                cardServiceFactory);
        when(cardServiceFactory.getCardServiceByType(CardType.CREDIT_CARD)).thenReturn(creditCardRestService);
        when(cardServiceFactory.getCardServiceByType(CardType.DEBIT_CARD)).thenReturn(debitCardRestService);
    }

    @Test
    public void getPowerOfAttorneysWithActiveAccountAndDebitCardCreditCardView()
    {
        final List<Authorization> authorizations = asList(DEBIT_CARD, CREDIT_CARD, VIEW);
        final List<CardReference> cardReferences = asList(
                new CardReference().setId(CARD_ID_1).setType(CardType.DEBIT_CARD),
                new CardReference().setId(CARD_ID_2).setType(CardType.CREDIT_CARD));
        final PowerOfAttorney expectedPOA = getPowerOfAttorney(authorizations, cardReferences);
        final Card expectedDebitCard = getDebitCard(Status.ACTIVE);
        final Account expectedAccount = getAccount(null);
        final Card expectedCreditCard = getCreditCard(Status.ACTIVE);
        mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
        mockPOA(expectedPOA);
        mockDebitCard(expectedDebitCard);
        mockCreditCard(expectedCreditCard);
        mockAccount(expectedAccount);

        final List<PowerOfAttorneyView> powerOfAttorneys = powerOfAttorneyService.getPowerOfAttorneys();

        assertEquals(1, powerOfAttorneys.size());
        final PowerOfAttorneyView actualPOA = powerOfAttorneys.get(0);
        assertEquals(expectedPOA.getGrantor(), actualPOA.getGrantor());
        assertEquals(expectedPOA.getGrantee(), actualPOA.getGrantee());
        assertEquals(expectedAccount, actualPOA.getAccount());
        assertEquals(2, actualPOA.getCards().size());
        assertTrue(actualPOA.getCards().contains(expectedCreditCard));
        assertTrue(actualPOA.getCards().contains(expectedDebitCard));
    }

    @Test
    public void getPowerOfAttorneysWithActiveAccountAndDebitCardView()
    {
        final List<Authorization> authorizations = asList(DEBIT_CARD, VIEW);
        final List<CardReference> cardReferences = asList(
                new CardReference().setId(CARD_ID_1).setType(CardType.DEBIT_CARD),
                new CardReference().setId(CARD_ID_2).setType(CardType.CREDIT_CARD));
        final PowerOfAttorney expectedPOA = getPowerOfAttorney(authorizations, cardReferences);
        final Card expectedCard = getDebitCard(Status.ACTIVE);
        final Account expectedAccount = getAccount(null);
        mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
        mockPOA(expectedPOA);
        mockDebitCard(expectedCard);
        mockAccount(expectedAccount);

        final List<PowerOfAttorneyView> powerOfAttorneys = powerOfAttorneyService.getPowerOfAttorneys();

        assertEquals(1, powerOfAttorneys.size());
        final PowerOfAttorneyView actualPOA = powerOfAttorneys.get(0);
        assertEquals(expectedPOA.getGrantor(), actualPOA.getGrantor());
        assertEquals(expectedPOA.getGrantee(), actualPOA.getGrantee());
        assertEquals(expectedAccount, actualPOA.getAccount());
        assertEquals(1, actualPOA.getCards().size());
        assertEquals(expectedCard, actualPOA.getCards().get(0));
        verifyZeroInteractions(creditCardRestService);
    }

    @Test
    public void getPowerOfAttorneysWithInactiveAccount()
    {
        final List<Authorization> authorizations = asList(DEBIT_CARD, VIEW);
        final List<CardReference> cardReferences = singletonList(
                new CardReference().setId(CARD_ID_1).setType(CardType.DEBIT_CARD));
        final PowerOfAttorney expectedPOA = getPowerOfAttorney(authorizations, cardReferences);
        mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
        mockPOA(expectedPOA);
        mockAccount(null);

        final List<PowerOfAttorneyView> powerOfAttorneys = powerOfAttorneyService.getPowerOfAttorneys();

        assertEquals(1, powerOfAttorneys.size());
        final PowerOfAttorneyView actualPOA = powerOfAttorneys.get(0);
        assertEquals(expectedPOA.getGrantor(), actualPOA.getGrantor());
        assertEquals(expectedPOA.getGrantee(), actualPOA.getGrantee());
        assertNull(actualPOA.getAccount());
        verifyZeroInteractions(creditCardRestService);
        verifyZeroInteractions(debitCardRestService);
    }

    @Test
    public void getPowerOfAttorneysWithActiveAccountAndInactiveDebitCard()
    {
        final List<Authorization> authorizations = asList(DEBIT_CARD, VIEW);
        final List<CardReference> cardReferences = asList(
                new CardReference().setId(CARD_ID_1).setType(CardType.DEBIT_CARD),
                new CardReference().setId(CARD_ID_2).setType(CardType.CREDIT_CARD));
        final PowerOfAttorney expectedPOA = getPowerOfAttorney(authorizations, cardReferences);
        final Account expectedAccount = getAccount(null);
        mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
        mockPOA(expectedPOA);
        mockAccount(expectedAccount);
        mockDebitCard(null);

        final List<PowerOfAttorneyView> powerOfAttorneys = powerOfAttorneyService.getPowerOfAttorneys();

        assertEquals(1, powerOfAttorneys.size());
        final PowerOfAttorneyView actualPOA = powerOfAttorneys.get(0);
        assertEquals(expectedPOA.getGrantor(), actualPOA.getGrantor());
        assertEquals(expectedPOA.getGrantee(), actualPOA.getGrantee());
        assertEquals(expectedAccount, actualPOA.getAccount());
        assertEquals(0, actualPOA.getCards().size());
        verify(debitCardRestService).getCard(CARD_ID_1);
        verifyZeroInteractions(creditCardRestService);
    }

    @Test(expected = NullPointerException.class)
    public void getPowerOfAttorneysNullFromReferenceEndpoint()
    {
        mockPowerOfAttorneys(null);

        powerOfAttorneyService.getPowerOfAttorneys();
    }

    @Test(expected = NullPointerException.class)
    public void getPowerOfAttorneysNullFromPoverOfAttorneyEndpoint()
    {
        mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
        mockPOA(null);

        powerOfAttorneyService.getPowerOfAttorneys();
    }

    private void mockAccount(final Account expectedAccount)
    {
        when(accountService.getAccount(ACCOUNT_ID)).thenReturn(Optional.ofNullable(expectedAccount));
    }

    private void mockDebitCard(final Card expectedCard)
    {
        when(debitCardRestService.getCard(CARD_ID_1)).thenReturn(Optional.ofNullable(expectedCard));
    }

    private void mockCreditCard(final Card expectedCard)
    {
        when(creditCardRestService.getCard(CARD_ID_2)).thenReturn(Optional.ofNullable(expectedCard));
    }

    private void mockPOA(final PowerOfAttorney expectedPOA)
    {
        when(restTemplate.getForObject(format(HTTP_LOCAL_HOST + POWER_OF_ATTORNEYS, POA_ID), PowerOfAttorney.class))
                .thenReturn(expectedPOA);
    }

    private PowerOfAttorney getPowerOfAttorney(
                                               final List<Authorization> authorizations,
                                               final List<CardReference> cardReferences)
    {
        return new PowerOfAttorney().setId(POA_ID).setGrantor(GRANTOR).setGrantee(GRANTEE).setAccount(ACCOUNT_ID)
                .setDirection(GIVEN).setAuthorizations(authorizations).setCards(cardReferences);
    }

    private void mockPowerOfAttorneys(final List result)
    {
        when(restTemplate.exchange(
                HTTP_LOCAL_HOST + POWER_OF_ATTORNEYS_REFERENCE,
                GET,
                null,
                new ParameterizedTypeReference<List<PowerOfAttorneyReference>>()
                {
                })).thenReturn(new ResponseEntity<>(result, HttpStatus.OK));
    }
}