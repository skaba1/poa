package org.orlenko.poa.integration;

import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.orlenko.poa.model.PowerOfAttorneyView;
import org.orlenko.poa.model.account.Account;
import org.orlenko.poa.model.card.Card;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpMethod.GET;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PowerOfAttorneyControllerIT {

	private static final String CONTENT_TYPE_HEADER = "Content-Type";
	private static final String CONTENT_TYPE_JSON = "application/json";
	private static final String URL = "http://localhost:%d/power-of-attorneys";

	@LocalServerPort
	private int port;
	private RestTemplate template = new RestTemplate();

	@Rule public final WireMockRule wireMockRule = new WireMockRule(options().port(8081).notifier(new ConsoleNotifier(true)));

	@Test
	public void getPowerOfAttorneys() {
		mockEndpoint("/power-of-attorneys", "poa.json");
		mockEndpoint("/power-of-attorneys/0001", "0001.json");
		mockEndpoint("/accounts/123456789", "123456789.json");
		mockEndpoint("/debit-cards/1111", "1111.json");
		mockEndpoint("/debit-cards/2222", "2222.json");

		ResponseEntity<List<PowerOfAttorneyView>> response = template.exchange(
			format(URL, port),
			GET,
			null,
			new ParameterizedTypeReference<List<PowerOfAttorneyView>>() {}
		);
		List<PowerOfAttorneyView> poaList = response.getBody();

		assertEquals(1, poaList.size());
		PowerOfAttorneyView powerOfAttorneyView = poaList.get(0);
		assertEquals("Super duper company", powerOfAttorneyView.getGrantor());
		assertEquals("Fellowship of the ring", powerOfAttorneyView.getGrantee());
		assertAccount(powerOfAttorneyView.getAccount());
		assertCards(powerOfAttorneyView.getCards());
	}

	private void assertCards(List<Card> cards) {
		assertEquals(2, cards.size());
		assertThat(cards, containsInAnyOrder(
			hasProperty("cardHolder", is("Frodo Basggins")),
			hasProperty("cardHolder", is("Aragorn"))
		));
	}

	private void assertAccount(Account account) {
		assertEquals("Super duper company", account.getOwner());
		assertEquals(750L, account.getBalance().longValue());
		assertEquals(LocalDate.parse("2007-10-12"), account.getCreated());
		assertNull(account.getEnded());
	}

	private void mockEndpoint(String url, String bodyFile) {
		wireMockRule.stubFor(get(urlMatching(url))
			.willReturn(aResponse()
				.withStatus(200)
				.withBodyFile(bodyFile)
				.withHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON)));
	}

}
